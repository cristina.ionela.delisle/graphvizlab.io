---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
name: tailtooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to the tail of an edge.

Used only if the edge has a [`tailURL`](#d:tailURL) attribute.
