---
defaults: []
flags:
- notdot
minimums: []
name: scale
types:
- double
- point
used_by: G
---
Scales layout by the given factor after the initial layout.

If only a single number is given, that number scales both width and height.
