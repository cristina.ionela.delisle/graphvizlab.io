---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
name: labeltooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to label of an edge.

Used only if the edge has a [`URL`](#d:URL) or [`labelURL`](#d:labelURL) attribute.
